def debugm(func):
    def wrap(self, *args, **kwargs):
        print(" [ DEBUG ] %s.%s: args=%s  kwargs=%s" %
            (type(self).__name__, func.__name__, args, kwargs))
        return func(self, *args, **kwargs)
    return wrap
