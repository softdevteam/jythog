from alice.tuprolog import Term, Int, Float, Long, Double, Struct, Var as ProVar
from jythog.objects import CoreTerm, Var as PyVar # name clashes

# ---------------------------
# Prolog -> Python Conversion
# ---------------------------

def py_int_of_pro_int(pro_int): return pro_int.intValue()
def py_float_of_pro_float(pro_float):
    return pro_float.doubleValue() # no such thing as single float in python
def py_float_of_pro_double(pro_double): return pro_double.doubleValue()
def py_long_of_pro_long(pro_long): return pro_long.intValue()

# atoms and composite terms coinside in tuprolog
def py_str_or_term_of_pro_struct(pro_struct):
    if pro_struct.getArity() == 0: # atom
        return str(pro_struct.getName())
    else:
        return CoreTerm(pro_struct)

def py_var_of_pro_var(pro_var):
    # This is hairy. Once a variable is bound it may not be replaced directly
    # with it's binding. Sometimes it remains a var, and from here we have
    # to ask what it was bound to.
    if pro_var.isBound():
        return py_of_pro(pro_var.getTerm())
    else: # it is an unbound var
        return PyVar(pro_var)

# Toplevel Prolog->Python Conversions
def py_of_pro(something):
    assert isinstance(something, Term)

    typ = type(something)
    if typ is Int:
        return py_int_of_pro_int(something)
    elif typ is Long:
        return py_long_of_pro_long(something)
    elif typ is Float:
        return py_float_of_pro_float(something)
    elif typ is Double:
        return py_float_of_pro_double(something)
    elif typ is Struct: # covers atoms too
        return py_str_or_term_of_pro_struct(something)
    elif typ is ProVar:
        return py_var_of_pro_var(something)
    else:
        raise NotImplementedError("Type conversion missing: %s" % (type(something)))

# ---------------------------
# Python -> Prolog Conversion
# ---------------------------

def pro_int_of_py_int(py_int): return Int(py_int)
def pro_double_of_py_float(py_float): return Double(py_float)
def pro_long_of_py_long(py_long): return Long(py_long)

def pro_var_of_py_var(py_var): return py_var.tu_var

def pro_struct_of_py_str_or_term(py_str_or_term):
    if isinstance(py_str_or_term, str): # atom
        return Struct(py_str_or_term)
    elif isinstance(py_str_or_term, CoreTerm): # Term
        return py_str_or_term.tu_struct # trivial unwrap
    else:
        raise TypeError("Bad term or atom conversion")

# Toplevel Python->Prolog Conversions
def pro_of_py(something):
    typ = type(something)
    if typ is int:
        return pro_int_of_py_int(something)
    elif typ is float:
        return pro_double_of_py_float(something)
    elif typ is long:
        return pro_long_of_py_long(something)
    elif typ is str:
        return pro_struct_of_py_str_or_term(something)
    elif typ is PyVar:
        return pro_var_of_py_var(something)
    elif typ is CoreTerm:
        return pro_struct_of_py_str_or_term(something)
    else:
        return pro_of_py_slow(something)

def pro_of_py_slow(something):
    if isinstance(something, int):
        return pro_int_of_py_int(something)
    elif isinstance(something, float):
        return pro_double_of_py_float(something)
    elif isinstance(something, long):
        return pro_long_of_py_long(something)
    elif isinstance(something, str):
        return pro_struct_of_py_str_or_term(something)
    elif isinstance(something, PyVar):
        return pro_var_of_py_var(something)
    elif isinstance(something, CoreTerm):
        return pro_struct_of_py_str_or_term(something)
    else:
        raise NotImplementedError("Type conversion missing: %s" % (type(something)))
