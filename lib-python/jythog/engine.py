#from org.jythog.interfaces import CoreEngineType
from java.lang import RuntimeException
from alice.tuprolog import Prolog, Theory, Struct, Var
from alice.tuprolog import NoSolutionException, NoMoreSolutionException, \
        InvalidTheoryException
from jythog.conversion import py_of_pro, pro_of_py
from jythog.objects import CoreTerm, Var as PyVar
#from jythog import PrologError

class CoreSolutionIterator(object):

    def __init__(self, core_engine, goal, free_vars):
        self.core_engine = core_engine
        self.goal = goal
        self.free_vars = free_vars
        self.first_iteration = True

    # Required by uni.py
    def finalise(self): pass

    def __iter__(self): return self

    def next(self):
        # first iteration sets up the query
        if self.first_iteration:
            tu_solinfo = self.core_engine.engine.solve(pro_of_py(self.goal))
            if not tu_solinfo.isSuccess(): # if no solutions atall
                raise StopIteration
            self.first_iteration = False
        else:
            # If there are no choice-points open, then there will be no
            # further solutions.
            if not self.core_engine.engine.hasOpenAlternatives():
                raise StopIteration

            tu_solinfo = self.core_engine.engine.solveNext()

            # If a choice point was open but we then find these paths to
            # be unsatisfiable.
            if not tu_solinfo.isSuccess():
                raise StopIteration

        return Solution(tu_solinfo, self.free_vars)

#class CoreEngine(CoreEngineType):
class CoreEngine(object):
    """ Wraps low-level access to and from Prolog """

    def __init__(self, db, local=None): # XXX local unused until reverse calls
        self.engine = Prolog()
        try:
            self.engine.setTheory(Theory(db));
        except InvalidTheoryException, e:
            from jythog import ParseError
            raise ParseError(e)

    @classmethod
    def from_file(cls, filename):
        # This check is to emulate unipycation's interface.
        if '\x00' in filename: raise TypeError("Null in filename")

        with open(filename, "r") as f: db = f.read()
        return cls(db)

    def _check_predicate_exists(self, goal):
        """ check a predicate of the correct arity exists """
        tm = self.engine.getTheoryManager()

        # Construct a term whose name is the goal name and whose arity is
        # the arity of the goal term. Avoid using conversion code for a
        # small speed up.
        v = Var()
        sig_term = Struct(goal.name, [ v for x in range(len(goal)) ])
        try:
            preds = tm.find(sig_term)
        except RuntimeException, e: # Usually a type error in tuprolog
            raise TypeError("bad types(?) when executing: %s/%d" %
                (goal.name, len(goal)))

        if preds.size() == 0:
            from jythog import PrologError
            raise PrologError("Unknown predicate: %s/%d" %
                (goal.name, len(goal)), goal)

    def query_single(self, goal, free_vars):
        it = self.query_iter(goal, free_vars)
        try:
            return it.next()
        except StopIteration:
            return None

    def query_iter(self, goal, free_vars):
        if not isinstance(goal, CoreTerm):
            raise TypeError("Bad goal, not Term: %s" %  (goal, ))

        if not isinstance(free_vars, list):
            raise TypeError("Free vars should be passed as a list")

        not_vars = [ x for x in free_vars if not isinstance(x, PyVar) ]
        if not_vars: raise TypeError("Free vars should be vars (duh!)")

        self._check_predicate_exists(goal)
        return CoreSolutionIterator(self, goal, free_vars)

class Solution(object):
    """ A wrapper around a solution.

    We may or may not need to build a mapping, Var -> Binding, depending
    upon whether the user is using the abstracted interface or the explicit
    interface. If the user needs only the abstracted interface, then only
    get_values_in_order() is called and we don't waste time building a dict.
    """

    def __init__(self, tu_solinfo, unbound_vars):
        self.unbound_vars = unbound_vars
        self.tu_solinfo = tu_solinfo
        self.result_dict = None # built lazily on request

    def _create_dict(self):
        if self.result_dict is None:
            self.result_dict = \
                { v : py_of_pro(self.tu_solinfo.getVarValue(v.tu_var.getName()))
                  for v in self.unbound_vars }

    def __len__(self): return len(self.unbound_vars)

    # used by explicit interface
    def __getitem__(self, key):
        self._create_dict()
        return self.result_dict[key]

    # used by abstracted interface
    def get_values_in_order(self):
        return tuple([
            py_of_pro(self.tu_solinfo.getVarValue(v.tu_var.getName())) for
            v in self.unbound_vars ])

    # for debugging only
    def __str__(self):
        self._create_dict()
        return str(self.result_dict)
