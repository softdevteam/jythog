# Types for Python app-level
# Try to closely simulate unipycation

from alice.tuprolog import Struct, Var as ProVar
from java.lang import ArrayIndexOutOfBoundsException

class CoreTerm(object):
    """ Represents a composite term.
        Note that unlike tuprolog, this term does not encapsulate atoms, as
        atoms become Python strings.
    """

    # The user will call with a name and args.
    # The interpreter will just wrap stuff and ignore the second arg.
    # XXX separate out two usage modes with a @classmethod, see swithog.
    def __init__(self, name_or_tu_struct, args=None):
        if isinstance(name_or_tu_struct, Struct):
            assert(name_or_tu_struct.getArity() > 0)
            # this is a trivial wrapping
            self.tu_struct = name_or_tu_struct
        else:
            assert(isinstance(args, list))
            from jythog.conversion import pro_of_py
            conv_args = [ pro_of_py(x) for x in args ]
            self.tu_struct = Struct(name_or_tu_struct, *conv_args)

    def __len__(self): return self.tu_struct.getArity()

    def __eq__(self, other):
        if not isinstance(other, CoreTerm): return False
        return self.tu_struct.isEqual(other.tu_struct)

    @property
    def name(self): return str(self.tu_struct.getName())

    @property
    def args(self):
        from jythog.conversion import py_of_pro
        tu = self.tu_struct
        return [ py_of_pro(tu.getArg(x)) for x in range(tu.getArity()) ]

    def __getitem__(self, key):
        from jythog.conversion import py_of_pro
        idx = int(key)
        if idx < 0:
            idx += self.tu_struct.getArity()

        # When unpacking a term python will look one elemnt past the end
        # to check if we have the correct number of arguments to unpack.
        # We must raise an IndexError if this is the case, i.e. catch the
        # Java exception and translate it.
        try:
            tu_arg = self.tu_struct.getArg(idx)
        except ArrayIndexOutOfBoundsException:
            raise IndexError("term argument out of range")

        return py_of_pro(tu_arg)

    def __str__(self):
        return str(self.tu_struct)

    def __repr__(self):
        return "CoreTerm(%r, %r)" % (self.name, self.args)

class Var(object):
    unique_int = 0

    def __init__(self, tu_var=None):
        if tu_var is not None:
            self.tu_var = tu_var
        else:
            # tuProlog identifies it's variables by name!
            # Any variable without a name is anonymous '_'!
            # So looks like we are going to have to generate a name.
            name = "_Pythog%d" % Var.unique_int
            Var.unique_int += 1
            self.tu_var = ProVar(name)

    def __str__(self):
        return str(self.tu_var)

    #def __eq__(self, other): return self.tu_var == other.tu_var

