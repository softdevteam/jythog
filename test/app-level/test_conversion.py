import unittest
from jythog.conversion import py_int_of_pro_int, py_float_of_pro_float, \
    py_float_of_pro_double, py_long_of_pro_long, py_of_pro, \
    py_str_or_term_of_pro_struct, py_var_of_pro_var
from jythog.conversion import pro_int_of_py_int, pro_double_of_py_float, \
        pro_long_of_py_long, pro_struct_of_py_str_or_term, \
        pro_var_of_py_var, pro_of_py

from alice.tuprolog import Int, Float, Long, Double, Struct, Var
from jythog.objects import Var as PyVar, CoreTerm

class TestConversion():

    FLOAT_CLOSE = 0.0001

    # -------------------------------
    # Test the specific py_*_of_pro_*
    # -------------------------------

    def test_py_int_of_pro_int(self):
        pro_int = Int(666)
        py_int = py_int_of_pro_int(pro_int)
        assert py_int == 666

    def test_py_float_of_pro_float(self):
        pro_float = Float(3.2)
        py_float = py_float_of_pro_float(pro_float)
        # differences between Java/Python/TuProlog floats cause rounding
        # errors, so we can just check we are within a tolerance (+/- 0.01)
        assert abs(3.2 - py_float) < TestConversion.FLOAT_CLOSE

    def test_py_float_of_pro_double(self):
        pro_double = Double(1.3)
        py_double = py_float_of_pro_double(pro_double)
        assert py_double == 1.3

    def test_py_long_of_pro_long(self):
        pro_long = Long(3)
        py_long = py_long_of_pro_long(pro_long)
        assert py_long == 3

    def test_py_long_of_pro_long2(self):
        # Long in java is a signed twos compliment 64-bit integer.
        # Weirdly, anything outside of this range is seen as a String.
        pro_long = Long(2147483647) # 2^31-1 (largest +ve long)
        py_long = py_long_of_pro_long(pro_long)
        assert py_long == 2147483647

    # test atoms
    def test_py_str_or_term_of_pro_struct(self):
        pro_atom = Struct("p") # atoms 0-ary structs
        py_str = py_str_or_term_of_pro_struct(pro_atom)
        assert type(py_str) is str
        assert py_str == "p"

    # test composite terms
    def test_py_str_or_term_of_pro_struct2(self):
        pro_term = Struct("p", Int(1), Int(2), Int(3)) # p(1, 2, 3)
        py_term = py_str_or_term_of_pro_struct(pro_term)
        assert len(py_term) == 3
        assert py_term.name == "p"
        assert py_term.args == [1, 2, 3]

    # test composite terms again, with nesting
    def test_py_str_or_term_of_pro_struct3(self):
        pro_term = Struct("p", Struct("q", Struct("r")))
        py_term = py_str_or_term_of_pro_struct(pro_term)

        assert len(py_term) == 1
        assert py_term.name == "p"

        arg = py_term[0]
        assert arg.name == "q"
        assert len(arg) == 1

        arg = arg[0]
        assert arg == "r" # i.e. atom

    def test_py_var_of_pro_var(self):
        pro_var = Var()
        py_var = py_var_of_pro_var(pro_var)
        assert isinstance(py_var, PyVar)

    # --------------------------
    # Test the generic py_of_pro
    # --------------------------

    def test_py_of_pro_int(self):
        assert py_of_pro(Int(666)), 666

    def test_py_of_pro_float(self):
        assert py_of_pro(Float(1.3)), 1.3

    def test_py_of_pro_double(self):
        assert py_of_pro(Double(3.1415)), 3.1415

    def test_py_of_pro_long(self):
        assert py_of_pro(Long(2147483647)) == 2147483647 # 2^31-1

    # atoms
    def test_py_pro_struct(self):
        pro_atom = Struct("foo")
        py_atom = py_of_pro(pro_atom)
        assert py_atom == "foo"

    # composite terms
    def test_py_pro_struct(self):
        pro_term = Struct("test", Int(1), Struct("l"), Double(5.5)) # test(1, l, 5.5)
        py_term = py_of_pro(pro_term)
        assert len(py_term) == 3
        assert py_term.name == "test"
        assert py_term.args == [1, "l", 5.5]

    def test_py_of_pro_var(self):
        pro_var = Var()
        py_var = py_of_pro(pro_var)
        assert isinstance(py_var, PyVar)

    # -------------------------------
    # Test the specific pro_*_of_py_*
    # -------------------------------

    def test_pro_int_of_py_int(self):
        pro_int = pro_int_of_py_int(777)
        assert isinstance(pro_int, Int)
        assert pro_int.intValue() == 777

    def test_pro_double_of_py_float(self):
        # See above re: almost equal
        pro_double = pro_double_of_py_float(2.3)
        assert isinstance(pro_double, Double)
        assert abs(pro_double.doubleValue() - 2.3) < TestConversion.FLOAT_CLOSE

    def test_pro_long_of_py_long(self):
        pro_long = pro_long_of_py_long(2147483647)
        assert isinstance(pro_long, Long)
        assert pro_long.longValue() == 2147483647

    def test_pro_struct_of_py_str_or_term(self):
        # In tuProlog atoms are 0-ary structs
        pro_atom = pro_struct_of_py_str_or_term("p")
        assert pro_atom.getArity() == 0
        assert pro_atom.getName() == "p"

    def test_pro_struct_of_py_str_or_term2(self):
        pro_term = pro_struct_of_py_str_or_term(CoreTerm("goodfor", ["war", "nothing"]))
        assert isinstance(pro_term, Struct)
        assert pro_term.getArity() == 2
        assert pro_term.getName() == "goodfor"
        assert pro_term.getArg(0).getName() == "war"
        assert pro_term.getArg(1).getName() == "nothing"

    # check nested terms works ok
    def test_pro_struct_of_py_str_or_term3(self):
        pro_term = pro_struct_of_py_str_or_term(CoreTerm("a", [CoreTerm("b", ["c"])]))

        assert isinstance(pro_term, Struct)
        assert pro_term.getName() == "a"
        assert pro_term.getArity() == 1
        arg = pro_term.getArg(0)

        assert isinstance(arg, Struct)
        assert arg.getName() == "b"
        assert arg.getArity() == 1
        arg = arg.getArg(0)

        assert isinstance(arg, Struct)
        assert arg.getName() == "c"
        assert arg.getArity() == 0

    def test_pro_var_of_py_var(self):
        pro_var = pro_var_of_py_var(PyVar())
        assert isinstance(pro_var, Var)

    # --------------------------
    # Test the generic py_of_pro
    # --------------------------

    def test_pro_of_py_int(self):
        assert pro_of_py(1337).intValue() == 1337

    def test_pro_of_py_float(self):
        pro_float = pro_of_py(1.23)
        assert abs(pro_float.floatValue() - 1.23) < TestConversion.FLOAT_CLOSE

    def test_pro_of_py_long(self):
        pro_long = pro_of_py(2**31-1)
        assert pro_long.intValue() == 2**31-1

    def test_pro_of_py_str(self):
        assert pro_of_py("abc").name == "abc"

    def test_pro_of_py_term(self):
        pro_term = pro_of_py(CoreTerm("p", ["x", "y"]))
        assert pro_term.getName() == "p"
        assert pro_term.getArity() == 2
        assert pro_term.getArg(0).getName() == "x"
        assert pro_term.getArg(1).getName() == "y"

    def test_pro_of_py_var(self):
        assert isinstance(pro_of_py(PyVar()), Var)

    def test_pro_of_py_subclass(self):
        class MyInt(int): pass
        assert pro_of_py(MyInt(1337)).intValue() == 1337

    def test_pro_of_py_subclass2(self):
        class MyStr1(str): pass
        class MyStr2(MyStr1): pass
        assert pro_of_py(MyStr2("abc")).name == "abc"
