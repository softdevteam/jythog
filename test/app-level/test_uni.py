# Pulled from the shared test repo
from unipycation_shared.tests.base_test_uni import BaseTestUni

from pytest import skip

def skipmethod(self):
    skip("jythog does not support that")

class TestUni(BaseTestUni):

    def setup_class(cls):
        import tempfile
        (cls.fd, cls.fname) = tempfile.mkstemp(prefix="unipycation-")

    test_pass_up_prolog_error = skipmethod
