# Pulled from the shared test repo
from unipycation_shared.tests.base_test_coreengine import BaseTestCoreEngine

from pytest import skip

def skipmethod(self):
    skip("jythog does not support that")

class TestCoreEngine(BaseTestCoreEngine):
    def setup_class(cls):
        import tempfile
        (cls.fd, cls.fname) = tempfile.mkstemp(prefix="unipycation-")

    test_from_file_error = skipmethod
    test_type_error_passed_up = skipmethod
    test_select = skipmethod
    test_error_in_database = skipmethod
    test_fail_in_database = skipmethod
