#!/usr/bin/env python2.7
import os, os.path, sys, importlib, sh

SCRIPT_PATH = os.path.abspath(__file__)
SCRIPT_DIR = os.path.dirname(SCRIPT_PATH)
DEPS_DIR = os.path.join(SCRIPT_DIR, "deps")
JYTHOG_BIN_DIR = os.path.join(SCRIPT_DIR, "bin")
UNI_SYMLINK_DIR = os.path.join(SCRIPT_DIR, "lib-python")
JYTHON_DIR = os.path.join(DEPS_DIR, "jython")

SHARED_VCS = "git"
SHARED_VERSION = "master"
SHARED_REPO = "git@bitbucket.org:softdevteam/unipycation-shared.git"
DEFAULT_SHARED_DIR = os.path.join(DEPS_DIR, "unipycation-shared")

TU_VCS = "svn"
TU_REPO = "http://tuprolog.googlecode.com/svn/2p/trunk/"
TU_VERSION = "1263"
TU_DIR = os.path.join(DEPS_DIR, "tuprolog")

JYTHON_VCS = "hg"
JYTHON_VERSION = "aa042b69bdda"
JYTHON_REPO = "http://hg.python.org/jython"

PATHS_CONF = os.path.join(SCRIPT_DIR, "paths.conf")

#
# FETCHING
#

def fetch_deps(with_shared=True):
    if not os.path.exists(DEPS_DIR):
        os.mkdir(DEPS_DIR)

    if with_shared: fetch_shared()
    fetch_jython()
    fetch_tuprolog()

def fetch_jython():
    import vcstools
    vcs = vcstools.get_vcs_client(JYTHON_VCS, JYTHON_DIR)
    if not os.path.exists(JYTHON_DIR):
        print("Cloning Jython: version=%s" % JYTHON_VERSION)
        vcs.checkout(JYTHON_REPO, version=JYTHON_VERSION)
    else:
        print("Updating Jython to version: %s" % JYTHON_VERSION)
        vcs.update(version=JYTHON_VERSION)

def fetch_tuprolog():
    import vcstools
    vcs = vcstools.get_vcs_client(TU_VCS, TU_DIR)
    if not os.path.exists(TU_DIR):
        print("Cloning tuProlog: version=%s" % TU_VERSION)
        vcs.checkout(TU_REPO, version=TU_VERSION)
    else:
        print("Updating tuProlog to version: %s" % TU_VERSION)
        vcs.update(version=TU_VERSION)

# used only for standalone bootstrap
def fetch_shared():
    import vcstools
    vcs = vcstools.get_vcs_client(SHARED_VCS, DEFAULT_SHARED_DIR)
    if not os.path.exists(DEFAULT_SHARED_DIR):
        print("Cloning fresh unipycation-shared: version=%s" % SHARED_VERSION)
        vcs.checkout(SHARED_REPO, version=SHARED_VERSION)
    else:
        print("Updating existing unipycation-shared to version: %s"
                % SHARED_VERSION)
        vcs.update(version=SHARED_VERSION, force_fetch=True)

#
# BUILDING
#

def make(target="all"):
    try:
        sh.gmake(target, _out=sys.stderr, _err=sys.stderr) # BSD
    except sh.CommandNotFound:
        try:
            sh.make(target, _out=sys.stderr, _err=sys.stderr) # linux
        except sh.CommandNotFound:
            raise CommandNotFound("Couldn't find gmake")

def build_deps():
    build_jython()
    build_tuprolog()

def build_jython():
    print "Building Jython..."
    os.chdir(JYTHON_DIR)
    sh.ant("all-jars", _out=sys.stdout, _err=sys.stderr)

def build_tuprolog():
    print "Building tuProlog..."
    os.chdir(os.path.join(TU_DIR, "ant"))
    sh.ant("08.package", _out=sys.stdout, _err=sys.stderr)

def build_jythog():
    os.chdir(SCRIPT_DIR)
    sh.make(_out=sys.stdout, _err=sys.stderr)
    hash_jars()

#
# CONFIGURATION
#

def configure(shared_dir=DEFAULT_SHARED_DIR):
    gen_paths_dot_conf(shared_dir)
    gen_uni_symlink(shared_dir)

def gen_paths_dot_conf(shared_dir):
    print("Generating paths.conf...")
    os.chdir(SCRIPT_DIR)
    with open(PATHS_CONF, "w") as conf:
        print >> conf, "TUPROLOG_PATH=%s" % TU_DIR
        print >> conf, "JYTHON_PATH=%s" % JYTHON_DIR
        print >> conf, "SHARED_PATH=%s" % shared_dir

def force_symlink(src, dest):
    if os.path.islink(dest):
        os.unlink(dest)
    os.symlink(src, dest)

# Run jythog for the first time so as to hash the jar files.
def hash_jars():
    print("Hashing jars...")
    sh.sh(os.path.join(SCRIPT_DIR, "hash_jars.sh"),
            _out=sys.stdout, _err=sys.stderr)

def gen_uni_symlink(shared_dir):
    print("Generating uni.py symlink...")
    uni_py_path = os.path.join(shared_dir, "unipycation_shared", "uni.py")
    target_path = os.path.join(UNI_SYMLINK_DIR, "uni.py")

    force_symlink(uni_py_path, target_path)

    # Remove old bytecode if there is one
    pyc_path = os.path.join(UNI_SYMLINK_DIR, "uni$py.class")
    try:
        os.unlink(pyc_path)
    except OSError:
        pass

def bootstrap(target, shared_dir=None):
    if shared_dir is None:
        with_shared = True
        shared_dir = DEFAULT_SHARED_DIR
    else:
        with_shared = False
        shared_dir = os.path.abspath(shared_dir)

    if target in ["fetch", "all"]:
        fetch_deps(with_shared)

    if target in ["build", "all"]:
        build_deps()

    # always configure
    configure(shared_dir)

    if target in ["build", "all"]:
        build_jythog()

#
# MAIN
#

def usage():
    print("""
Usage:
  bootstrap.py target [unipycation_shared_path]

If no shared path specified, will clone afresh
Valid targets are: fetch, build, configure, all""")
    sys.exit(1)

if __name__ == "__main__":
    try:
        shared_arg = os.path.abspath(sys.argv[2])
    except IndexError:
        shared_arg = None

    try:
        target = sys.argv[1]
    except IndexError:
        usage()

    if target not in ["fetch", "build", "configure", "all"]:
        print("Bad target")
        usage()

    if shared_arg is not None:
        bootstrap(target, shared_arg)
    else:
        bootstrap(target)
