# Jythog

A language composition of Python and Prolog built on the JVM.

## Directory structure

  * lib_python/: Python application-level code
  * org/: Interpreter-level Java code.
  * app.py: Top level pythog interpreter (don't run this though).
  * bin/jythog: Wrapper around app.py, run this.
  * test/: Unit tests (using the unittest module).
  * bootstrap.sh: auto-setup script.

## Bootstrapping

To bootstrap standalone, run:

```
python2.7 bootstrap.py all
```

You will need hg, svn, git, ant and the python modules 'sh' and 'vcstools'.

If you are looking to bootstrap the other unipycation VMs too, then use the
universal bootstrapper, as found here:
https://bitbucket.org/softdevteam/unipycation-shared

## Usage

To run Jythog, run `bin/jythog`.

## Tests

Run 'make test' to run unit tests.
