package org.jythog.util;

import org.jythog.interfaces.CoreEngineType;
import org.python.core.PyObject;
import org.python.core.PyString;
import org.python.util.PythonInterpreter;

public class CoreEngineFactory {

    private PyObject coreEngineClass;

    public CoreEngineFactory(PythonInterpreter interp) {
        System.out.println("Factory constructor");
        interp.exec("from jythog.engine import CoreEngine");
        coreEngineClass = interp.get("CoreEngine");
    }

    public CoreEngineType create(String dbStr) {
        PyString db = new PyString(dbStr);
        PyObject coreEngineObject = coreEngineClass.__call__(db);
        return (CoreEngineType) coreEngineObject.__tojava__(CoreEngineType.class);
    }

}
