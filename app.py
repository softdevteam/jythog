# This is the bootstrap code for Jythog.
#
# Since there is no easy way to get a reference to the running
# PythonInterpreter, we instantiate a new one by calling Java
# (thus obtaining a reference) and then do all our work in there.
#
# The PythonInterpreter is fairly lightweight, so this is acceptable :)

import sys, os, os.path, argparse
from org.python.util import PythonInterpreter, InteractiveConsole
from org.python.core import Py, PySystemState
from java.lang import System

# Ensures sys.executable is what you might expect.
# For now assumes jythog is run out of source dir.
def set_sys_executable(pi):
        pi.systemState.executable = \
                os.path.abspath(os.path.join(__file__, "..", "bin", "jythog"))

def add_pythonpath(pi):
    pi.exec("""
import sys, os
if os.getenv('PYTHONPATH'):
    sys.path.extend(os.getenv('PYTHONPATH').split(':'))
    """)

if __name__ == "__main__":

    if len(sys.argv) == 0:
        # wrapper execs as `jython app.py <args>` so always >1 arg
        raise RunTimeError("Please run this under the 'jythog' wrapper script")

    sys.argv = sys.argv[1:] # strip app.py

    # argparse assumes atleast one arg is in sys.argv, crashes...
    if len(sys.argv) > 0:
        parser = argparse.ArgumentParser(
            #epilog=EPILOG,
            #description=DESCR,
            #formatter_class=argparse.RawDescriptionHelpFormatter
        )

        # We don't pretend to support all of Python's switches, but -m is useful.
        parser.add_argument("-m", nargs='?', help="run a module as per PEP 338")
        parser.add_argument("argv", nargs=argparse.REMAINDER, help="run a module as per PEP 338")

        args = parser.parse_args(sys.argv)
    else:
        class NoArgs: # pretend we parsed and got no args
            def __init__(self):
                self.argv = []
                self.m = None
        args = NoArgs()

    # Instantiate the interpreter
    if len(args.argv) == 0 and args.m is None:
        pi = InteractiveConsole()
        pi.systemState.argv = [''] # emulating cpython
        set_sys_executable(pi)
        add_pythonpath(pi)
        banner = pi.getDefaultBanner()
        banner = "Jythog, a Python + Prolog language composition\n" + \
                "(c) 2013 Software Development Team, King's College London\n" + banner
        pi.interact(banner, sys.stdin)
    else:
        pi = PythonInterpreter()
        set_sys_executable(pi)

        if args.m is not None: # -m was passed execute module as a script
            pi.systemState.argv = [args.m] + args.argv
            pi.exec("import runpy");
            pi.set("name", Py.newString(args.m))
            pi.exec("runpy.run_module(name, run_name='__main__', alter_sys=True)")
        else:
            # Add the path of the script to PYTHONPATH
            pi.exec("import sys, os.path")
            pi.exec("sys.path.append(os.path.dirname(os.path.abspath(sys.argv[0])))")
            add_pythonpath(pi)

            # Set __file__
            pi.exec("__file__ = '%s'" % sys.argv[0])

            pi.systemState.argv = args.argv # strip app.py
            pi.execfile(sys.argv[0])

    pi.cleanup()
