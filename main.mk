SRCS=	org/jythog/util/CoreEngineFactory.java \
	org/jythog/interfaces/CoreEngineType.java

JYTHON_CLASSPATH=	${JYTHON_PATH}/dist/jython.jar
TUPROLOG_CLASSPATH=	${TUPROLOG_PATH}/src:/opt/tuprolog-2.7.2/lib/ikvm-api.jar:${TUPROLOG_PATH}/src:/opt/tuprolog-2.7.2/lib/mscorlib.jar:${TUPROLOG_PATH}/build/archives/2p.jar:${TUPROLOG_PATH}/build/archives/tuprolog.jar
EXTRA_CLASSPATH=	lib-python:${JYTHON_CLASSPATH}:${TUPROLOG_CLASSPATH}

all:
	javac -classpath ${EXTRA_CLASSPATH} ${SRCS}

.PHONY: test
test:
	${PWD}/bin/jythog bin/py.test.jythog test

clean:
	find org -type f -name '*.class' | xargs rm -f
	find jythog -name '*\$py.class' | xargs rm -f
